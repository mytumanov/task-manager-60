package ru.mtumanov.tm.api.model;

import org.jetbrains.annotations.NotNull;

public interface IHaveName {

    @NotNull
    String getName();

    void setName(@NotNull String name);

}
