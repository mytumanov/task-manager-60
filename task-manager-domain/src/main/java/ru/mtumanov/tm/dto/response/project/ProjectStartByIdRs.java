package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.ProjectDTO;

@NoArgsConstructor
public class ProjectStartByIdRs extends AbstractProjectRs {

    public ProjectStartByIdRs(@Nullable final ProjectDTO project) {
        super(project);
    }

    public ProjectStartByIdRs(@Nullable final Throwable err) {
        super(err);
    }

}
