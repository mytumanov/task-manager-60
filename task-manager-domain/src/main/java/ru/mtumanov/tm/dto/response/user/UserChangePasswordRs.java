package ru.mtumanov.tm.dto.response.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserChangePasswordRs extends AbstractUserRs {

    public UserChangePasswordRs(@Nullable final UserDTO user) {
        super(user);
    }

    public UserChangePasswordRs(@NotNull final Throwable err) {
        super(err);
    }

}