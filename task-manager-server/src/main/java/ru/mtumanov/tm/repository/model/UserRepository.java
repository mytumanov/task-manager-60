package ru.mtumanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.api.repository.model.IUserRepository;
import ru.mtumanov.tm.model.User;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void clear() {
        @NotNull final List<User> users = findAll();
        for (@NotNull final User user : users) {
            remove(user);
        }
    }

    @Override
    @NotNull
    public List<User> findAll() {
        return entityManager.createQuery("FROM User p", User.class).getResultList();
    }


    @Override
    @NotNull
    public List<User> findAll(@NotNull final Comparator<User> comparator) {
        return entityManager.createQuery("FROM User p ORDER BY p." + getComporator(comparator), User.class).getResultList();
    }

    @Override
    @Nullable
    public User findOneById(@NotNull final String id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(1) FROM User p", Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final User project = findOneById(id);
        remove(project);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) {
        return entityManager.createQuery("FROM User p WHERE login = :login", User.class)
                .setParameter("login", login)
                .getSingleResult();
    }

    @Override
    @NotNull
    public User findByEmail(@NotNull final String email) {
        return entityManager.createQuery("FROM User p WHERE email = :email", User.class)
                .setParameter("email", email)
                .getSingleResult();
    }

    @Override
    public boolean isLoginExist(@NotNull final String login) {
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExist(@NotNull final String email) {
        return findByEmail(email) != null;
    }

}
