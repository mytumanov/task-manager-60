package ru.mtumanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.api.repository.model.ITaskRepository;
import ru.mtumanov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    private static final String USER_ID = "userId";

    @NotNull
    private static final String ID = "id";

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Task> tasks = findAll();
        for (@NotNull final Task task : tasks)
            remove(task);
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId) {
        return entityManager.createQuery("FROM Task p WHERE p.user.id = :userId", Task.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @NotNull
    public List<Task> findAll(@NotNull final String userId, @NotNull final Comparator<Task> comparator) {
        return entityManager.createQuery("FROM Task p WHERE p.user.id = :userId ORDER BY p." + getComporator(comparator), Task.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @Nullable
    public Task findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("FROM Task p WHERE p.user.id = :userId AND p.id = :id", Task.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(1) FROM Task p WHERE p.user.id = :userId", Long.class)
                .setParameter(USER_ID, userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Task task = findOneById(userId, id);
        entityManager.refresh(task);
        task.getUser().getTasks().remove(task);
        task.getProject().getTasks().remove(task);
        remove(task);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public void clear() {
        @NotNull final List<Task> tasks = findAll();
        for (@NotNull final Task task : tasks)
            remove(task);
    }

    @Override
    @NotNull
    public List<Task> findAll() {
        return entityManager.createQuery("FROM Task p", Task.class)
                .getResultList();
    }


    @Override
    @NotNull
    public List<Task> findAll(@NotNull final Comparator<Task> comparator) {
        return entityManager.createQuery("FROM Task p ORDER BY p." + getComporator(comparator), Task.class)
                .getResultList();
    }

    @Override
    @Nullable
    public Task findOneById(@NotNull final String id) {
        return entityManager.find(Task.class, id);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(1) FROM Task p", Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final Task task = findOneById(id);
        entityManager.refresh(task);
        task.getUser().getTasks().remove(task);
        task.getProject().getTasks().remove(task);
        remove(task);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return entityManager.createQuery("FROM Task t WHERE t.user.id = :userId AND t.project.id = :projectId", Task.class)
                .setParameter(USER_ID, userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

}
