package ru.mtumanov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.dto.IDtoRepository;
import ru.mtumanov.tm.comparator.CreatedComparator;
import ru.mtumanov.tm.comparator.NameComparator;
import ru.mtumanov.tm.comparator.StatusComparator;
import ru.mtumanov.tm.dto.model.AbstractModelDTO;
import ru.mtumanov.tm.exception.AbstractException;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Comparator;

public abstract class AbstractDtoRepository<M extends AbstractModelDTO> implements IDtoRepository<M> {

    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    public EntityManager getEntityManager() {
        return entityManager;
    }

    @Nullable
    protected String getComporator(@NotNull final Comparator comparator) {
        if (comparator instanceof CreatedComparator)
            return "created";
        else if (comparator instanceof NameComparator)
            return "name";
        else if (comparator instanceof StatusComparator)
            return "status";
        else
            return null;
    }


    @Override
    public void add(@NotNull final M entity) throws AbstractException {
        entityManager.persist(entity);
    }

    @Override
    @NotNull
    public M update(@NotNull final M entity) throws AbstractException {
        return entityManager.merge(entity);
    }


}
