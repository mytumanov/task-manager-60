package ru.mtumanov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.api.repository.model.IProjectRepository;
import ru.mtumanov.tm.model.Project;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public class ProjectRepository extends AbstractUserOwnedRepository<Project> implements IProjectRepository {

    @NotNull
    private static final String USER_ID = "userId";

    @NotNull
    private static final String ID = "id";

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<Project> projects = findAll(userId);
        for (@NotNull final Project project : projects) {
            remove(project);
        }
    }

    @Override
    @NotNull
    public List<Project> findAll(@NotNull final String userId) {
        return entityManager.createQuery("FROM Project p WHERE p.user.id = :userId", Project.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @NotNull
    public List<Project> findAll(@NotNull final String userId, @NotNull final Comparator<Project> comparator) {
        return entityManager.createQuery("FROM Project p WHERE p.user.id = :userId ORDER BY p." + getComporator(comparator), Project.class)
                .setParameter(USER_ID, userId)
                .getResultList();
    }

    @Override
    @Nullable
    public Project findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("FROM Project p WHERE p.user.id = :userId AND p.id = :id", Project.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(1) FROM Project p WHERE p.user.id = :userId", Long.class)
                .setParameter(USER_ID, userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        @NotNull final Project project = findOneById(userId, id);
        entityManager.refresh(project);
        project.getUser().getProjects().remove(project);
        remove(project);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public void clear() {
        @NotNull final List<Project> projects = findAll();
        for (@NotNull final Project project : projects) {
            remove(project);
        }
    }

    @Override
    @NotNull
    public List<Project> findAll() {
        return entityManager.createQuery("FROM Project p", Project.class)
                .getResultList();
    }


    @Override
    @NotNull
    public List<Project> findAll(@NotNull final Comparator<Project> comparator) {
        return entityManager.createQuery("FROM Project p ORDER BY p." + getComporator(comparator), Project.class)
                .getResultList();
    }

    @Override
    @Nullable
    public Project findOneById(@NotNull final String id) {
        @Nullable final Project project = entityManager.find(Project.class, id);
        return project;
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(1) FROM Project p", Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        @NotNull final Project project = findOneById(id);
        remove(project);
    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

}
