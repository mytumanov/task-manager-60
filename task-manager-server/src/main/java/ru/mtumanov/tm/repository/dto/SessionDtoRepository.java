package ru.mtumanov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.mtumanov.tm.api.repository.dto.IDtoSessionRepository;
import ru.mtumanov.tm.dto.model.SessionDTO;

import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public class SessionDtoRepository extends AbstractUserOwnedDtoRepository<SessionDTO> implements IDtoSessionRepository {

    @NotNull
    private static final String USER_ID = "userId";

    @NotNull
    private static final String ID = "id";

    @Override
    public void clear(@NotNull final String userId) {
        entityManager.createQuery("DELETE FROM SessionDTO WHERE userId = :userId")
                .setParameter(USER_ID, userId)
                .executeUpdate();

    }

    @Override
    @NotNull
    public List<SessionDTO> findAll(@NotNull final String userId) {
        return entityManager.createQuery("FROM SessionDTO p WHERE p.userId = :userId", SessionDTO.class)
                .setParameter(USER_ID, userId)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    @NotNull
    public List<SessionDTO> findAll(@NotNull final String userId, @NotNull final Comparator<SessionDTO> comparator) {
        return entityManager
                .createQuery("FROM SessionDTO p WHERE p.userId = :userId ORDER BY p." + getComporator(comparator),
                        SessionDTO.class)
                .setParameter(USER_ID, userId)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    @Nullable
    public SessionDTO findOneById(@NotNull final String userId, @NotNull final String id) {
        return entityManager.createQuery("FROM SessionDTO p WHERE p.userId = :userId AND p.id = :id", SessionDTO.class)
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .setMaxResults(1).getSingleResult();
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return entityManager.createQuery("SELECT COUNT(1) FROM SessionDTO p WHERE p.userId = :userId", Long.class)
                .setParameter(USER_ID, userId)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        entityManager.createQuery("DELETE FROM SessionDTO p WHERE p.userId = :userId AND p.id = :id")
                .setParameter(USER_ID, userId)
                .setParameter(ID, id)
                .executeUpdate();

    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return findOneById(userId, id) != null;
    }

    @Override
    public void clear() {
        entityManager.createQuery("DELETE FROM SessionDTO")
                .executeUpdate();

    }

    @Override
    @NotNull
    public List<SessionDTO> findAll() {
        return entityManager.createQuery("FROM SessionDTO p", SessionDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    @NotNull
    public List<SessionDTO> findAll(@NotNull final Comparator<SessionDTO> comparator) {
        return entityManager.createQuery("FROM SessionDTO p ORDER BY p." + getComporator(comparator), SessionDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .getResultList();
    }

    @Override
    @Nullable
    public SessionDTO findOneById(@NotNull final String id) {
        return entityManager.find(SessionDTO.class, id);
    }

    @Override
    public long getSize() {
        return entityManager.createQuery("SELECT COUNT(1) FROM SessionDTO p", Long.class)
                .getSingleResult();
    }

    @Override
    public void removeById(@NotNull final String id) {
        entityManager.createQuery("DELETE FROM SessionDTO WHERE id = :id")
                .setParameter(ID, id)
                .executeUpdate();

    }

    @Override
    public boolean existById(@NotNull final String id) {
        return findOneById(id) != null;
    }

}
