package ru.mtumanov.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.mtumanov.tm.dto.model.UserDTO;

public interface IDtoUserRepository extends IDtoRepository<UserDTO> {

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

    boolean isLoginExist(@NotNull String login);

    boolean isEmailExist(@NotNull String email);

}
