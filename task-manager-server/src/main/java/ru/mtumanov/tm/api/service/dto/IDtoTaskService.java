package ru.mtumanov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.exception.AbstractException;

import java.util.List;

public interface IDtoTaskService extends IDtoUserOwnedService<TaskDTO> {

    @NotNull
    List<TaskDTO> findAllByProjectId(@NotNull String userId, @NotNull String projectId) throws AbstractException;

    @NotNull
    TaskDTO create(@NotNull String userId, @NotNull String name, @NotNull String description) throws AbstractException;

    @NotNull
    TaskDTO updateById(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description) throws AbstractException;

    @NotNull
    TaskDTO changeTaskStatusById(@NotNull String userId, @NotNull String id, @NotNull Status status) throws AbstractException;

}
