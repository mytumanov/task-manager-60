package ru.mtumanov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.api.repository.dto.IDtoUserOwnedRepository;
import ru.mtumanov.tm.api.service.dto.IDtoUserOwnedService;
import ru.mtumanov.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.mtumanov.tm.exception.AbstractException;

import javax.transaction.Transactional;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractDtoUserOwnedService<M extends AbstractUserOwnedModelDTO, R extends IDtoUserOwnedRepository<M>>
        extends AbstractDtoService<M, R> implements IDtoUserOwnedService<M> {

    @Override
    @NotNull
    @Transactional
    public M add(@NotNull final String userId, @NotNull final M model) throws AbstractException {
        model.setUserId(userId);
        repository.add(model);
        return model;
    }

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        repository.clear(userId);
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) {
        return repository.existById(userId, id);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        return repository.findAll(userId);
    }

    @Override
    @NotNull
    public List<M> findAll(@NotNull final String userId, @Nullable final Comparator<M> comparator) {
        if (comparator == null)
            return repository.findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @Override
    @NotNull
    public M findOneById(@NotNull final String userId, @NotNull final String id) {
        return repository.findOneById(userId, id);
    }

    @Override
    public long getSize(@NotNull final String userId) {
        return repository.getSize(userId);
    }

    @Override
    @Transactional
    public void remove(@NotNull final String userId, @NotNull final M model) {
        repository.removeById(userId, model.getId());
    }

    @Override
    @Transactional
    public void removeById(@NotNull final String userId, @NotNull final String id) {
        repository.removeById(userId, id);
    }

}
