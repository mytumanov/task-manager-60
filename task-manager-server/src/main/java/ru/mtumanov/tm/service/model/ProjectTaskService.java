package ru.mtumanov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.mtumanov.tm.api.repository.model.IProjectRepository;
import ru.mtumanov.tm.api.repository.model.ITaskRepository;
import ru.mtumanov.tm.api.service.model.IProjectTaskService;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.ProjectNotFoundException;
import ru.mtumanov.tm.exception.field.IdEmptyException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;

import javax.transaction.Transactional;

public class ProjectTaskService implements IProjectTaskService {

    @NotNull
    @Autowired
    private ITaskRepository taskRepository;

    @NotNull
    @Autowired
    private IProjectRepository projectRepository;

    @Override
    @Transactional
    public void bindTaskToProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!projectRepository.existById(userId, projectId))
            throw new ProjectNotFoundException();


        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        @NotNull final Task task = taskRepository.findOneById(userId, taskId);
        task.setProject(project);
        taskRepository.update(task);
    }

    @Override
    @Transactional
    public void removeProjectById(@NotNull final String userId, @NotNull final String projectId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (!projectRepository.existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final Project project = projectRepository.findOneById(userId, projectId);
        projectRepository.remove(project);
    }

    @Override
    @Transactional
    public void unbindTaskFromProject(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskId) throws AbstractException {
        if (projectId.isEmpty())
            throw new IdEmptyException();
        if (taskId.isEmpty())
            throw new IdEmptyException();
        if (!projectRepository.existById(userId, projectId))
            throw new ProjectNotFoundException();

        @NotNull final Task task = taskRepository.findOneById(userId, taskId);
        task.setProject(null);
        taskRepository.update(task);
    }

}
