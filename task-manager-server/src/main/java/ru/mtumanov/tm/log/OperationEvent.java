package ru.mtumanov.tm.log;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class OperationEvent {

    @NotNull
    private final Long timestamp = System.currentTimeMillis();
    
    @Nullable
    private OperationType type;
    @Nullable
    private Object entity;
    @Nullable
    private String table;

    public OperationEvent(@NotNull final Object entity, @NotNull final OperationType type) {
        this.type = type;
        this.entity = entity;
    }

}
