package ru.mtumanov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.mtumanov.tm.dto.request.project.ProjectChangeStatusByIdRq;
import ru.mtumanov.tm.dto.response.project.ProjectChangeStatusByIdRs;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.event.ConsoleEvent;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.field.StatusNotSupportedException;
import ru.mtumanov.tm.util.TerminalUtil;

import java.util.Arrays;

@Component
public class ProjectChangeStatusByIdListener extends AbstractProjectListener {

    @Override
    @NotNull
    public String getDescription() {
        return "Change project status by id";
    }

    @Override
    @NotNull
    public String getName() {
        return "project-change-status-by-id";
    }

    @Override
    @EventListener(condition = "@projectChangeStatusByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws AbstractException {
        System.out.println("[COMPLETE PROJECT BY ID]");
        System.out.println("ENTER ID:");
        final String id = TerminalUtil.nextLine();
        System.out.println("ENTER STAUS:");
        System.out.println(Arrays.toString(Status.values()));
        @NotNull final String statusValue = TerminalUtil.nextLine();
        @Nullable final Status status = Status.toStatus(statusValue);
        if (status == null)
            throw new StatusNotSupportedException();
        @NotNull final ProjectChangeStatusByIdRq request = new ProjectChangeStatusByIdRq(getToken(), id, status);
        @NotNull final ProjectChangeStatusByIdRs response = getProjectEndpoint().projectChangeStatusById(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
