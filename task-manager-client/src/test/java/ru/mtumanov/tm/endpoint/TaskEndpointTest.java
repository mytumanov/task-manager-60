package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.endpoint.IAuthEndpoint;
import ru.mtumanov.tm.api.endpoint.IProjectEndpoint;
import ru.mtumanov.tm.api.endpoint.ITaskEndpoint;
import ru.mtumanov.tm.dto.model.ProjectDTO;
import ru.mtumanov.tm.dto.model.TaskDTO;
import ru.mtumanov.tm.dto.request.project.ProjectCreateRq;
import ru.mtumanov.tm.dto.request.task.*;
import ru.mtumanov.tm.dto.request.user.UserLoginRq;
import ru.mtumanov.tm.dto.response.project.ProjectCreateRs;
import ru.mtumanov.tm.dto.response.task.*;
import ru.mtumanov.tm.dto.response.user.UserLoginRs;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.marker.SoapCategory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final static String host = "localhost";

    @NotNull
    private final static String port = "6060";

    @NotNull
    private final static ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);

    @NotNull
    private final static IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);

    @NotNull
    private final static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final static String login = "NOT_COOL_USER";

    @NotNull
    private final static String password = "Not Cool";

    @NotNull
    private List<TaskDTO> taskList;

    @NotNull
    private static String token;

    @NotNull
    private static ProjectDTO project;

    @BeforeClass
    public static void login() {
        @NotNull final UserLoginRq request = new UserLoginRq(login, password);
        @NotNull final UserLoginRs response = authEndpoint.login(request);
        token = response.getToken();

        @NotNull final ProjectCreateRs projectResponse = projectEndpoint
                .projectCreate(new ProjectCreateRq(token, "TEST Project", "Test description"));
        project = projectResponse.getProject();
    }

    @Before
    public void initData() {
        taskList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskCreateRs response = taskEndpoint.taskCreate(new TaskCreateRq(token, "TEST Task " + i, "Test description " + i));
            taskList.add(response.getTask());
        }
    }

    @After
    public void clearData() {
        taskEndpoint.taskClear(new TaskClearRq(token));
    }

    @Test
    public void testTaskBindToProject() {
        @NotNull final TaskCreateRs taskResponse = taskEndpoint.taskCreate(new TaskCreateRq(token, "TEST Task to bind", "Test description"));
        taskEndpoint.taskBindToProject(new TaskBindToProjectRq(token, taskResponse.getTask().getId(), project.getId()));
        @NotNull final TaskShowByIdRs response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskResponse.getTask().getId()));
        assertEquals(project.getId(), response.getTask().getProjectId());
    }

    @Test
    public void testErrTaskBindToProject() {
        @NotNull final TaskCreateRs taskResponse = taskEndpoint.taskCreate(new TaskCreateRq(token, "TEST Task to bind", "Test description"));
        @NotNull TaskShowByIdRs response;
        @NotNull TaskBindToProjectRs bindResponse;

        bindResponse = taskEndpoint.taskBindToProject(new TaskBindToProjectRq(token, "", project.getId()));
        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskResponse.getTask().getId()));
        assertNull(response.getTask().getProjectId());
        assertFalse(bindResponse.getSuccess());
        assertNotNull(bindResponse.getMessage());

        bindResponse = taskEndpoint.taskBindToProject(new TaskBindToProjectRq(token, taskResponse.getTask().getId(), ""));
        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskResponse.getTask().getId()));
        assertNull(response.getTask().getProjectId());
        assertFalse(bindResponse.getSuccess());
        assertNotNull(bindResponse.getMessage());

        bindResponse = taskEndpoint.taskBindToProject(new TaskBindToProjectRq("", taskResponse.getTask().getId(), project.getId()));
        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskResponse.getTask().getId()));
        assertNull(response.getTask().getProjectId());
        assertFalse(bindResponse.getSuccess());
        assertNotNull(bindResponse.getMessage());
    }

    @Test
    public void testTaskChangeStatusById() {
        for (@NotNull final TaskDTO task : taskList) {
            @NotNull final TaskChangeStatusByIdRs response = taskEndpoint
                    .taskChangeStatusById(new TaskChangeStatusByIdRq(token, task.getId(), Status.IN_PROGRESS));
            assertTrue(response.getSuccess());
            assertNotEquals(task.getStatus(), response.getTask().getStatus());
        }
    }

    @Test
    public void testErrTaskChangeStatusById() {
        @NotNull TaskChangeStatusByIdRs response;

        response = taskEndpoint
                .taskChangeStatusById(new TaskChangeStatusByIdRq("", "123", Status.IN_PROGRESS));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint
                .taskChangeStatusById(new TaskChangeStatusByIdRq(token, "", Status.IN_PROGRESS));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskClear() {
        @NotNull TaskShowByIdRs taskRs;
        taskRs = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskList.get(0).getId()));
        assertNotNull(taskRs.getTask());

        @NotNull final TaskClearRs response = taskEndpoint.taskClear(new TaskClearRq(token));
        assertTrue(response.getSuccess());

        taskRs = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskList.get(0).getId()));
        assertNull(taskRs.getTask());
    }

    @Test
    public void testErrTaskClear() {
        @NotNull final TaskClearRs response = taskEndpoint.taskClear(new TaskClearRq(""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskCompleteById() {
        for (@NotNull final TaskDTO task : taskList) {
            @NotNull final TaskCompleteByIdRs response = taskEndpoint.taskCompleteById(new TaskCompleteByIdRq(token, task.getId()));
            assertEquals(Status.COMPLETED, response.getTask().getStatus());
            assertTrue(response.getSuccess());
        }
    }

    @Test
    public void testErrTaskCompleteById() {
        @NotNull TaskCompleteByIdRs response;
        response = taskEndpoint.taskCompleteById(new TaskCompleteByIdRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskCompleteById(new TaskCompleteByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskCreate() {
        @NotNull final String name = "Create task";
        @NotNull final String description = "description";
        @NotNull final TaskCreateRs response = taskEndpoint
                .taskCreate(new TaskCreateRq(token, name, description));
        assertEquals(name, response.getTask().getName());
        assertEquals(description, response.getTask().getDescription());
        assertEquals(Status.NOT_STARTED, response.getTask().getStatus());
    }

    @Test
    public void testErrTaskCreate() {
        @NotNull TaskCreateRs response;
        response = taskEndpoint.taskCreate(new TaskCreateRq("", "name", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskCreate(new TaskCreateRq(token, "", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskList() {
        @NotNull final TaskListRs response = taskEndpoint.taskList(new TaskListRq(token, null));
        assertEquals(taskList, response.getTasks());
    }

    @Test
    public void testErrTaskList() {
        @NotNull final TaskListRs response = taskEndpoint.taskList(new TaskListRq("", null));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskRemoveById() {
        for (@NotNull final TaskDTO task : taskList) {
            taskEndpoint.taskRemoveById(new TaskRemoveByIdRq(token, task.getId()));
            @NotNull final TaskShowByIdRs response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, task.getId()));
            assertNull(response.getTask());
        }
    }

    @Test
    public void testErrTaskRemoveById() {
        @NotNull TaskShowByIdRs response;
        response = taskEndpoint.taskShowById(new TaskShowByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testErrTaskRemoveByIndex() {
        @NotNull TaskShowByIdRs response;
        response = taskEndpoint.taskShowById(new TaskShowByIdRq("", "132"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskShowById() {
        for (@NotNull final TaskDTO task : taskList) {
            @NotNull final TaskShowByIdRs response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, task.getId()));
            assertEquals(task, response.getTask());
        }
    }

    @Test
    public void testErrTaskShowById() {
        @NotNull TaskShowByIdRs response;
        response = taskEndpoint.taskShowById(new TaskShowByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskShowByProjectId() {
        @NotNull final TaskCreateRs taskResponse = taskEndpoint.taskCreate(new TaskCreateRq(token, "TEST Task to unbind", "Test description"));
        @NotNull TaskShowByProjectIdRs response;
        response = taskEndpoint.taskShowByProjectId(new TaskShowByProjectIdRq(token, project.getId()));
        assertNull(response.getTasks());
        taskEndpoint.taskBindToProject(new TaskBindToProjectRq(token, taskResponse.getTask().getId(), project.getId()));
        response = taskEndpoint.taskShowByProjectId(new TaskShowByProjectIdRq(token, project.getId()));
        assertNotNull(response.getTasks());
    }

    @Test
    public void testErrTaskShowByProjectId() {
        @NotNull TaskShowByProjectIdRs response;
        response = taskEndpoint.taskShowByProjectId(new TaskShowByProjectIdRq("", project.getId()));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskShowByProjectId(new TaskShowByProjectIdRq(token, ""));
        assertTrue(response.getSuccess());
        assertNull(response.getTasks());
    }

    @Test
    public void testTaskStartById() {
        for (@NotNull final TaskDTO task : taskList) {
            @NotNull final TaskStartByIdRs response = taskEndpoint.taskStartById(new TaskStartByIdRq(token, task.getId()));
            assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
        }
    }

    @Test
    public void testErrTaskStartById() {
        @NotNull TaskStartByIdRs response;
        response = taskEndpoint.taskStartById(new TaskStartByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskStartById(new TaskStartByIdRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskUnbindFromProject() {
        @NotNull final TaskCreateRs taskResponse = taskEndpoint.taskCreate(new TaskCreateRq(token, "TEST Task to unbind", "Test description"));
        taskEndpoint.taskBindToProject(new TaskBindToProjectRq(token, taskResponse.getTask().getId(), project.getId()));

        taskEndpoint.taskUnbindFromProject(new TaskUnbindFromProjectRq(token, taskResponse.getTask().getId(), project.getId()));
        @NotNull final TaskShowByIdRs response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskResponse.getTask().getId()));
        assertNull(response.getTask().getProjectId());
    }

    @Test
    public void testErrTaskUnbindFromProject() {
        @NotNull TaskUnbindFromProjectRs response;
        response = taskEndpoint.taskUnbindFromProject(new TaskUnbindFromProjectRq("", "132", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskUnbindFromProject(new TaskUnbindFromProjectRq(token, "", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskUnbindFromProject(new TaskUnbindFromProjectRq(token, "123", ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskUpdateById() {
        for (final TaskDTO task : taskList) {
            @NotNull final String name = task.getName() + " TEST";
            @NotNull final String description = task.getDescription() + " TEST";
            @NotNull final TaskUpdateByIdRs response = taskEndpoint.taskUpdateById(new TaskUpdateByIdRq(token, task.getId(), name, description));
            assertEquals(name, response.getTask().getName());
            assertEquals(description, response.getTask().getDescription());
        }
    }

    @Test
    public void testErrTaskUpdateById() {
        @NotNull TaskUpdateByIdRs response;
        response = taskEndpoint.taskUpdateById(new TaskUpdateByIdRq("", "123", "name", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskUpdateById(new TaskUpdateByIdRq(token, "", "name", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskUpdateById(new TaskUpdateByIdRq(token, "123", "", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

}
